# -*- coding: utf-8 -*-
import re
import urllib.request

from bs4 import BeautifulSoup

from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter

import requests #주소를 위도, 경도로 변한
import json #주소 위도, 경도로 변한

from haversine import haversine #거리계산

SLACK_TOKEN = '' # 자기 슬랙 봇 토큰 입력
SLACK_SIGNING_SECRET = ''


app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

#xml 데이터 불러오기
shelter_list = [] #대피소 이름
shelterSe_list = [] #대피소 유형
aceptncCo_list = [] #수용인원
shelterPhoneNumber_list = [] #전화번호
rdnmadr_list = [] #주소
latitude_list = [] #위도
hardness_list = [] #경도

#공공데이터 오픈 Api
url = 'http://api.data.go.kr/openapi/erthqktdlwavshltr-std?serviceKey=w6CKv5nhtg0bC4XrVz90YrvtEjN4r9UldrWZWTg%2B8B%2Fp4UydfSpYHSE4T1DPN4ECyRIBsQjpVTI9v7cp5PVhWQ%3D%3D&pageNo=1&numOfRows=5001&type=xml'

#오픈 api로 받아온 공공데이터를 알맞게 파싱
uffile = urllib.request.urlopen(url)
contents = uffile.read().decode('utf-8')
items = re.findall(r'<item>.+?</item>',contents,re.DOTALL)

for item in items:
  
    shelterNm = re.search(r'<shelterNm>(.+)</shelterNm>', item) # 대피소명 데이터 가져오기
    shelterSe = re.search(r'<shelterSe>(.+)</shelterSe>', item) # 유형구분 데이터 가져오기
    aceptncCo = re.search(r'<aceptncCo>(.+)</aceptncCo>', item) # 최대수용인원 데이터 가져오기
    shelterPhoneNumber = re.search(r'<shelterPhoneNumber>(.+)</shelterPhoneNumber>', item) # 전화번호 데이터 가져오기
    rdnmadr = re.search(r'<rdnmadr>(.+)</rdnmadr>', item) # 도로명 주소 데이터 가져오기     
    latitude = re.search(r'<latitude>(.+)</latitude>', item) # 위도 데이터 가져오기
    hardness = re.search(r'<hardness>(.+)</hardness>', item) # 경도 데이터 가져오기

    #리스트에 넣기(파싱된 데이터)
    shelter_list += shelterNm.group(1).split('\n') 
    shelterSe_list += shelterSe.group(1).split('\n')
    aceptncCo_list += aceptncCo.group(1).split('\n')
    shelterPhoneNumber_list += shelterPhoneNumber.group(1).split('\n')

    # 3개의 항목에서 group(태그제거)과정에서 None을 리턴하기 때문에 예외처리를 해야함
    # None을 리턴할 때 공백 및 0을 대입, 아닐경우 데이터를 대입
    if rdnmadr is None:
        rdnmadr_list += " "
    else:
        rdnmadr_list += rdnmadr.group(1).split('\n')

    if latitude is None:
        latitude_list += "0"
    else:
        latitude_list += latitude.group(1).split('\n')

    if hardness is None:
        hardness_list += "0"
    else:
        hardness_list += hardness.group(1).split('\n')
    

# 크롤링 함수 구현하기
def _crawl_portal_keywords(text):

    address_find  = text[13:] #슬랙에서 입력된 주소값을 해당 범위만큼 가져옵니다.
    
    #입력된 주소값을 vworld OpenApi를 이용해서 (위도,경도)값으로 변경
    URL_geocoder='http://api.vworld.kr/req/address'
    crs_find = 'EPSG:4326' 
    PARAMS_geocoder={'service':'address', 'request':'getCoord', 'version':'2.0', 'format':'json', 'type':'road', 'address':address_find, 'refine':'true', 'simple':'false', 'crs':crs_find, 'key':'9C41DC76-B78E-3484-90F0-690466165693'}
    r_geocoder=requests.get(url=URL_geocoder, params=PARAMS_geocoder)

    result_coder=json.loads(r_geocoder.text)

    # a = 위도(입력받은 주소) / b = 경도(입력받은 주소)
    a = result_coder['response']['result']['point']['y']
    b = result_coder['response']['result']['point']['x']

    # 변환된 위도와 경도값을 실수화 (최단거리 계산하기 위해)
    a = float(a)
    b = float(b)

    result = 999999 # 최단 거리 계산을 뽑아내기 위해 초기값 설정

    # 입력받은 데이터(위도,경도) 와 저장된 데이터(위도,경도)를 활용하여 최단거리 계산 및 해당 인덱스 리턴
    for i in range(0,len(shelter_list)):

        c = float(latitude_list[i]) #저장된 데이터(위도)값을 실수화하여 불러온다.
        d = float(hardness_list[i]) #저장된 데이터(경도)값을 실수화하여 불러온다.

        input_ad = (a, b) # 입력받은 변환된 데이터(위도, 경도)
        search_ad = (c, d) # 저장된 공공 데이터(위도, 경도)

        distance = haversine(input_ad, search_ad) * 1000 # haversine을 이용하여 데이터(위도, 경도)를 비교하여 거리를 계산

        result_1 = float(distance) #데이터(위도, 경도)간의 거리를 실수화 하여 저장

        if result > result_1:

            result = result_1 # 계속 비교하여 최단거리(가장 가까운 대피소)를 업데이트
            info = i # 가장 가까운 대피소의 인덱스값 저장

    # 가장 가까운 대피소의 인덱스값을 바탕으로 필요한 정보를 제공
    result_info = '대피소명 : ' + shelter_list[info] + ' / ' + '유형 : ' + shelterSe_list[info] + ' / ' + '주소(도로명) : ' + rdnmadr_list[info] + ' / ' + '수용인원 : ' + aceptncCo_list[info] + ' / ' +'전화번호 : ' + shelterPhoneNumber_list[info]
    return result_info


# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    
    keywords = _crawl_portal_keywords(text)
    slack_web_client.chat_postMessage(
        channel=channel,
        text=keywords
    )

# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"

if __name__ == '__main__':
    app.run('0.0.0.0', port=8080)
